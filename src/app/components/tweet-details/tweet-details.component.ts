import {Component, Input, OnInit} from '@angular/core';
import {Tweet} from "../../models/tweet";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-tweet-details',
  templateUrl: './tweet-details.component.html',
  styleUrls: ['./tweet-details.component.scss']
})
export class TweetDetailsComponent implements OnInit {

  @Input()
  tweet: Tweet

  constructor(private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
        this.tweet = JSON.parse(params['tweet']);
        console.log('Got param: ', this.tweet.content);

      }
    )
  }

  ngOnInit(): void {
  }

}
