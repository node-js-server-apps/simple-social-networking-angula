import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Subscription} from "rxjs";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CreateTweetRequest} from "../../models/create-tweet-request";
import {CreateTweetService} from "../../services/tweets/create-tweet.service";
import {Tweet} from "../../models/tweet";

@Component({
  selector: 'app-create-tweet',
  templateUrl: './create-tweet.component.html',
  styleUrls: ['./create-tweet.component.scss']
})
export class CreateTweetComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  form: FormGroup;
  isLoading = false;
  fileName: String;
  selectedImage = '';
  selectedFile: File | null = null
  toggled: boolean = false;
  message: string = '';
  @Output()
  tweetCreatedEvent: EventEmitter<Tweet> = new EventEmitter<Tweet>();

  constructor(private fb: FormBuilder,
              private createTweetService: CreateTweetService) {
    this.form = this.fb.group({
      tweetContent: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(180)
      ]),
      image: new FormControl('')
    });
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.isLoading = !this.isLoading;
    const tweetContent = this.form.get('tweetContent')?.value;
    const tweetImage = this.form.get('image')?.value;
    const hashtags = this.findHashtags(tweetContent);
    const newTweet = new CreateTweetRequest();
    newTweet.content = tweetContent.trim();
    if (hashtags.length > 0) {
      newTweet.tags = hashtags;
    }
    console.log(newTweet);
    if (this.selectedFile) {
      this.createMultiMediaTweet(newTweet, this.selectedFile);
    } else {
      this.createPlainTweet(newTweet);
    }

  }

  createPlainTweet(newTweet: CreateTweetRequest) {
    this.createTweetService.createTweet(newTweet)
      .subscribe(resp => {
        this.isLoading = !this.isLoading;
        console.log('tweet created succesfully');
        console.log(resp);
        this.form.reset();
        this.selectedImage = '';
        this.tweetCreatedEvent.emit(resp.tweet);
      }, error => {
        this.isLoading = !this.isLoading;
        console.log('errro creating tweet');
        console.log(error);
      })
  }

  createMultiMediaTweet(newTweet: CreateTweetRequest, file: File) {
    this.createTweetService.createMultiMediaTweet(newTweet, file)
      .subscribe(resp => {
        this.isLoading = !this.isLoading;
        console.log('multimedia tweet created successfully');
        console.log(resp);
        this.form.reset();
        this.selectedImage = '';
        this.tweetCreatedEvent.emit(resp.tweet);
      }, error => {
        this.isLoading = !this.isLoading;
        console.log('errro creating tweet');
        console.log(error);
      })
  }

  findHashtags(searchText: string) {
    const regexp = /(\s|^)\#\w\w+\b/gm;
    let result = searchText.match(regexp);
    if (result) {
      result = result.map(function (s) {
        return s.trim().replace('#', '');
      });
      return result;
    } else {
      return [];
    }
  }

  onFileSelected(event: any) {
    const reader = new FileReader();
    const file: File = event.target.files[0];

    if (file) {
      this.selectedFile = file;
      this.fileName = file.name;

      const formData = new FormData();

      formData.append("thumbnail", file);

      console.log('file: ' + this.fileName);
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.selectedImage = reader.result as string;
        // console.log(this.selectedImage);
        this.form.patchValue({
          image: reader.result
        });
      };
    }
  }

  ngOnDestroy(): void {
    if (this.subscription !== undefined) {
      this.subscription.unsubscribe();
    }
  }

  removeImageWithTweet() {
    this.selectedImage = '';
    this.selectedFile = null;
  }
}
