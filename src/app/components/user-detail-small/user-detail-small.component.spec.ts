import {ComponentFixture, TestBed} from '@angular/core/testing';

import {UserDetailSmallComponent} from './user-detail-small.component';

describe('UserDetailSmallComponent', () => {
  let component: UserDetailSmallComponent;
  let fixture: ComponentFixture<UserDetailSmallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserDetailSmallComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
