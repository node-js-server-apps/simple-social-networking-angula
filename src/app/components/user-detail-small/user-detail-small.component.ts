import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserFollowUnfollowService} from "../../services/user-connection/user.follow-unfollow.service";
import {User} from "../../models/user";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-user-detail-small',
  templateUrl: './user-detail-small.component.html',
  styleUrls: ['./user-detail-small.component.scss']
})
export class UserDetailSmallComponent implements OnInit {

  ACTION_TYPE_FOLLOW = 'FOLLOW';
  ACTION_TYPE_FOLLOW_BACK = 'FOLLOW_BACK';
  ACTION_TYPE_UNFOLLOW = 'UNFOLLOW';


  @Input()
  actionType: String;

  @Input()
  user: User;

  @Output()
  followUserEvent: EventEmitter<User> = new EventEmitter<User>();
  @Output()
  unFollowUserEvent: EventEmitter<User> = new EventEmitter<User>();

  btnAction: String = '';
  btnHidden = false;
  isApiCallRunning = false;

  constructor(private followUnfollowService: UserFollowUnfollowService) {
  }

  ngOnInit(): void {
    if (this.actionType === this.ACTION_TYPE_FOLLOW) {
      this.btnAction = 'Follow';
    } else if (this.actionType === this.ACTION_TYPE_UNFOLLOW) {
      this.btnAction = 'Unfollow';
    } else if (this.actionType === this.ACTION_TYPE_FOLLOW_BACK) {
      this.btnAction = 'Follow Back';
    }
  }


  doFollowOrUnfollow() {
    if (this.actionType === this.ACTION_TYPE_FOLLOW || this.actionType == this.ACTION_TYPE_FOLLOW_BACK) {
      this.doFollow();
    } else if (this.actionType === this.ACTION_TYPE_UNFOLLOW) {
      this.doUnfollow()
    } else {
      console.error('undefined action');
    }
  }

  doFollow() {
    this.isApiCallRunning = true;
    this.followUnfollowService
      .followUser(this.user.id)
      .subscribe(resp => {
        console.log('successfully user: ' + this.user.username);
        this.followUserEvent.emit(this.user);
        this.isApiCallRunning = false;
      }, error => {
        this.isApiCallRunning = false;
      });

  }

  doUnfollow() {
    this.isApiCallRunning = true;
    this.followUnfollowService
      .unFollowUser(this.user.id)
      .subscribe(resp => {
        this.unFollowUserEvent.emit(this.user);
        this.isApiCallRunning = false;
      }, error => {
        this.isApiCallRunning = false;
      });
  }

  getUserPicture() {
    return environment.resourceUrl + "/users/profile-images/" + this.user.mediaId;
  }

}
