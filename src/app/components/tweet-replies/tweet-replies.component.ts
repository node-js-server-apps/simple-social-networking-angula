import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Reply} from "../../models/reply";
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {User} from "../../models/user";
import {UserService} from "../../services/user/user.service";
import {ReplyDeleteService} from "../../services/tweets/reply-delete.service";
import {Subscription} from "rxjs";
import {AlertUi} from "../../models/alert.ui";

@Component({
  selector: 'app-tweet-replies',
  templateUrl: './tweet-replies.component.html',
  styleUrls: ['./tweet-replies.component.scss']
})
export class TweetRepliesComponent implements OnInit {
  closeModal: string;
  @Input()
  reply: Reply;
  @Input()
  tweetId: string;
  loggedInUser: User
  @Output()
  deletedTweetReplyEvent: EventEmitter<Reply> = new EventEmitter<Reply>();
  alertErrorDelete: AlertUi = {show: false, type: 'error', message: ''}
  isDeleting = false;
  private subscription: Subscription;

  constructor(private modalService: NgbModal,
              private replyDeleteService: ReplyDeleteService,
              private userService: UserService) {
  }

  ngOnInit(): void {
    this.subscription = this.userService.getUserDetailsSubject()
      .subscribe(user => {
        this.loggedInUser = user;
      });
  }

  deletedTweetReply(modalData: any) {
    console.log('delete tweet reply: ' + this.reply.content);
    this.modalService.open(modalData).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
      this.isDeleting = true;
      console.log(res);
      this.doDeleteTweetReply();
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
      console.log('other reason: ' + res);
    });
  }

  doDeleteTweetReply() {
    this.isDeleting = true;
    this.replyDeleteService.deleteTweetReply(this.tweetId, this.reply._id)
      .subscribe(resp => {
        this.isDeleting = true;
        this.deletedTweetReplyEvent.emit(this.reply);
        this.alertErrorDelete = {show: false, type: 'error', message: ''}
      }, error => {
        this.isDeleting = false;
        console.log('error while deleting tweet reply');
        this.isDeleting = false;
        this.alertErrorDelete = {show: true, type: 'error', message: 'Please check internet connection'};
      })
  }


  isReplyOwnedByCurrentUser() {
    return this.reply.user_id === this.loggedInUser?.id;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


}
