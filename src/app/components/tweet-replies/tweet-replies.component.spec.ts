import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TweetRepliesComponent} from './tweet-replies.component';

describe('TweetRepliesComponent', () => {
  let component: TweetRepliesComponent;
  let fixture: ComponentFixture<TweetRepliesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TweetRepliesComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TweetRepliesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
