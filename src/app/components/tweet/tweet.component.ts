import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Tweet} from "../../models/tweet";
import {UserService} from "../../services/user/user.service";
import {Subscription} from "rxjs";
import {User} from "../../models/user";
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {TweetDeleteService} from "../../services/tweets/tweet-delete.service";
import {Reply} from "../../models/reply";
import {AlertUi} from "../../models/alert.ui";
import {TweetLikeUnlikeService} from "../../services/tweets/tweet-like-unlike.service";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.scss'],
})
export class TweetComponent implements OnInit, OnDestroy {
  @Input()
  tweet: Tweet;
  @Output()
  deleteTweetEvent: EventEmitter<Tweet> = new EventEmitter();
  loggedInUser: User
  closeModal: string;
  public isDeleting = false;
  public isCollapsed = true;
  private subscription: Subscription;
  replies: Reply[];
  likes: User[];
  alertErrorDelete: AlertUi = {show: false, type: 'error', message: ''}
  isLiked = false;

  constructor(private modalService: NgbModal,
              private tweetDeleteService: TweetDeleteService,
              private userService: UserService,
              private tweetLikeService: TweetLikeUnlikeService) {

  }

  ngOnInit(): void {
    this.replies = this.tweet.replies;
    this.likes = this.tweet.likes;
    this.subscription = this.userService.getUserDetailsSubject()
      .subscribe(user => {
        this.loggedInUser = user;
        this.likes.filter(u => {
          this.isLiked = u.id === this.loggedInUser.id;
        })
      });
  }

  deleteTweet(modalData: any) {
    console.log('delete tweet: ' + this.tweet.content);
    this.modalService.open(modalData).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
      this.doDeleteTweet(this.tweet);
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
      console.log('other reason: ' + res);
    });
  }

  doDeleteTweet(tweet: Tweet) {
    this.isDeleting = true;
    this.tweetDeleteService.deleteTweet(tweet.id)
      .subscribe(response => {
        if (response && response.status === 200) {
          this.isDeleting = false;
          console.log('delete response');
          console.log(response);
          this.deleteTweetEvent.emit(this.tweet);
          this.alertErrorDelete = {show: false, type: 'error', message: ''}
        } else {
          this.alertErrorDelete = {
            show: true,
            type: 'error',
            message: 'Cannot deleting tweet'
          };
        }
      }, error => {
        console.log(error);
        this.isDeleting = false;
        this.alertErrorDelete = {show: true, type: 'error', message: 'Please check internet connection'};
      })
  }

  isTweetOwnedByCurrentUser() {

    // console.log(this.tweet.created_by.id + ' : ' + this.tweet.content);
    // console.log(this.loggedInUser.id);
    // console.log(this.tweet.created_by.id === this.loggedInUser.id);
    return this.tweet.created_by?.id === this.loggedInUser?.id;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  removeRemoveReplyFromList(reply: Reply) {
    console.log('remove tweet from list before size: ' + this.replies.length);
    console.log('remove tweet from list: ' + reply.content);
    console.log('remove tweet from list: ' + JSON.stringify(reply));
    this.replies = this.replies.filter(x => {
      console.log('x id: ' + x.id);
      console.log('reply id: ' + reply.id);
      return x._id !== reply._id
    });
    console.log('remove tweet from list: ' + this.replies.length);
  }

  onTweetReplyCreated(reply: Reply) {
    this.replies.push(reply);
  }


  doLikeOrUnlike() {
    if (this.isLiked) {
      this.tweetLikeService.unLikeTweet(this.tweet.id)
        .subscribe(resp => {
          this.isLiked = !this.isLiked;
          this.likes = this.likes.filter(x => {
            return x.id !== this.loggedInUser.id;
          });
        }, error => {
          console.log(error);
        });
    } else {
      this.tweetLikeService.likeTweet(this.tweet.id)
        .subscribe(resp => {
          this.isLiked = !this.isLiked;
          this.likes.push(this.loggedInUser);
        }, error => {
          console.log(error);
        });
    }
  }

  getMyMedia() {
    if (this.tweet.media.length > 0) {
      return environment.resourceUrl + "/tweets/media/" + this.tweet.media[0];
    } else {
      return undefined;
    }
  }

  getMyPicture() {
    return environment.resourceUrl + "/users/profile-images/" + this.tweet.created_by.mediaId;
  }

  ngOnDestroy(): void {
    if (this.subscription !== undefined) {
      this.subscription.unsubscribe();
    }
  }

}
