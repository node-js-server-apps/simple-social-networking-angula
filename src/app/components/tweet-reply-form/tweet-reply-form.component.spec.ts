import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TweetReplyFormComponent} from './tweet-reply-form.component';

describe('TweetReplyComponent', () => {
  let component: TweetReplyFormComponent;
  let fixture: ComponentFixture<TweetReplyFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TweetReplyFormComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TweetReplyFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
