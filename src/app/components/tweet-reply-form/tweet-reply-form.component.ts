import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Reply} from "../../models/reply";
import {Tweet} from "../../models/tweet";
import {ReplyTweetService} from "../../services/tweets/reply-tweet.service";
import {UserService} from "../../services/user/user.service";
import {User} from "../../models/user";

@Component({
  selector: 'app-tweet-reply-form',
  templateUrl: './tweet-reply-form.component.html',
  styleUrls: ['./tweet-reply-form.component.scss']
})
export class TweetReplyFormComponent implements OnInit {
  @Input()
  tweet: Tweet;
  isLoading = false;
  form: FormGroup;
  user: User;
  @Output()
  tweetReplyCreatedEvent: EventEmitter<Reply> = new EventEmitter<Reply>();

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private replyTweetService: ReplyTweetService) {
    this.form = this.fb.group({
      replyToTweet: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(180)
      ])
    });
  }

  ngOnInit(): void {
    this.userService.getUserDetailsSubject()
      .subscribe(user => {
        this.user = user;
      })
  }

  onSubmit() {
    this.isLoading = !this.isLoading;
    const replyToTweet = this.form.get('replyToTweet')?.value;
    const reply = new Reply();
    reply.content = replyToTweet.trim();
    console.log(reply);

    this.replyTweetService.replyToTweet(this.tweet.id, reply)
      .subscribe(resp => {
        this.isLoading = !this.isLoading;
        console.log('reply created succesfully');
        console.log(resp.reply);
        this.form.reset();
        this.tweetReplyCreatedEvent.emit(resp.reply);
      }, error => {
        this.isLoading = !this.isLoading;
        console.log('errro creating reply to: ' + this.tweet.id);
        console.log(error);
      })

  }
}
