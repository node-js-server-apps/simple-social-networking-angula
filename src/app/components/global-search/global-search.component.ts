import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/debounceTime';
// import 'rxjs/add/operator/distinctUntilChanged';
// import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-global-search',
  templateUrl: './global-search.component.html',
  styleUrls: ['./global-search.component.scss']
})
export class GlobalSearchComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }


  suggest(string: HTMLInputElement | undefined) {
    if (string == undefined) {
      return;
    }
    this.router.navigateByUrl('search/text?query=' + string.value);
  }
}

