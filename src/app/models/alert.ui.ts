export interface AlertUi {
  show: boolean;
  type: string;
  message: string;
}
