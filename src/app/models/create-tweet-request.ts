export class CreateTweetRequest {
  content: string;
  tags: string[];
}
