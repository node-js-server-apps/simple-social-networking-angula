export class User {
  id: string;
  name: string;
  username: string;
  mediaId: string;
}
