import {Tweet} from "./tweet";

export class CreateTweetResponse {
  message: string;
  tweet: Tweet;
}
