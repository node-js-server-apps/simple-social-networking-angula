export class Reply {
  id: string;
  user_id: string;
  name: string;
  username: string;
  content: string;
  createdAt: Date
  _id: string
}
