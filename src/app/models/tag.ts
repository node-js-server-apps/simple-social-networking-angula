export class Tag {
  id: string;
  name: string;

  public toString(): string {
    return '#' + this.name;
  }
}
