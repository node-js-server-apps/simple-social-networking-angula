import {Reply} from "./reply";

export class CreateReplyResponse {
  reply: Reply;
}
