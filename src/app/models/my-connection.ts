import {User} from "./user";

export class MyConnection {
  user: User;
  followers: [User];
  followings: [User];
}
