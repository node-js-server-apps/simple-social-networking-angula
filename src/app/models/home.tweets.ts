import {Tweet} from "./tweet";

export class HomeTweets {
  message: string;
  tweets: Tweet[];
  size: number;
}
