export class TokenResponse {
  id: string;
  name: string;
  username: string;
  message: string
  token: string;
}
