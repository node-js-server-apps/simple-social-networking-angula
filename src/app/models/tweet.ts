import {User} from "./user";
import {Tag} from "./tag";
import {Reply} from "./reply";

export class Tweet {
  id: string
  content: string;
  created_by: User
  tags: Tag[]
  replies: Reply[];
  likes: User[];
  createdAt: Date
  media: String[]
}
