import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from "./features/login/login.component";
import {HomeComponent} from "./features/home/home.component";
import {MyTweetsComponent} from "./features/my-tweets/my-tweets.component";
import {AuthGuardService} from "./services/auth/auth.guard.service";
import {TimelineComponent} from "./features/timeline/timeline.component";
import {ProfileComponent} from "./features/profile/profile.component";
import {TagSearchComponent} from "./features/tag-search/tag-search.component";
import {RegisterComponent} from "./features/register/register.component";
import {SettingsComponent} from "./features/settings/settings.component";
import {TweetDetailsComponent} from "./components/tweet-details/tweet-details.component";
import {FollowersComponent} from "./features/followers/followers.component";
import {NotFoundLandingComponent} from "./features/not-found-landing/not-found-landing.component";
import {FollowingsComponent} from "./features/followings/followings.component";
import {WhoToFollowSuggestionsComponent} from "./features/who-to-follow-suggestions/who-to-follow-suggestions.component";

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/home'},
  {path: 'search', component: TagSearchComponent},
  {path: 'search/tags', component: TagSearchComponent},
  {path: 'search/text', component: TagSearchComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'settings', component: SettingsComponent, canActivate: [AuthGuardService]},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuardService]},
  {path: 'my-tweets', component: MyTweetsComponent, canActivate: [AuthGuardService]},
  {path: 'timeline', component: TimelineComponent, canActivate: [AuthGuardService]},
  {path: 'my-tweets', component: MyTweetsComponent, canActivate: [AuthGuardService]},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService]},
  {path: 'followers', component: FollowersComponent, canActivate: [AuthGuardService]},
  {path: 'followings', component: FollowingsComponent, canActivate: [AuthGuardService]},
  {path: 'suggestions', component: WhoToFollowSuggestionsComponent, canActivate: [AuthGuardService]},
  {path: 'tweet-details', component: TweetDetailsComponent, canActivate: [AuthGuardService]},
  {path: '**', component: NotFoundLandingComponent, canActivate: [AuthGuardService]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
