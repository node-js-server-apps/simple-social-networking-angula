import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {StorageService} from "../storage/storage.service";
import {UserConnectionResponse} from "../../models/user-connection-response";
import {MyConnection} from "../../models/my-connection";


@Injectable({providedIn: "root"})
export class UserConnectionService {

  private userConnectionSubject = new Subject<MyConnection>();

  constructor(private httpClient: HttpClient,
              private storageService: StorageService) {
  }

  public getUserConnectionSubject(): Observable<MyConnection> {
    return this.userConnectionSubject.asObservable();
  }

  getMyConnection() {
    this.httpClient
      .get<UserConnectionResponse>(environment.resourceUrl + '/user-connections')
      .subscribe(response => {
        this.userConnectionSubject.next(response.my_connection);
      });
  }

}
