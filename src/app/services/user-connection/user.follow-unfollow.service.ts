import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {FollowUnfollowResponse} from "../../models/follow.unfollow.response";


@Injectable({providedIn: "root"})
export class UserFollowUnfollowService {

  constructor(private httpClient: HttpClient) {
  }


  followUser(targetUserId: String) {
    return this.httpClient
      .post<FollowUnfollowResponse>(environment.resourceUrl + '/user-connections/follow', {targetUserId: targetUserId})
  }

  unFollowUser(targetUserId: String) {
    return this.httpClient
      .post<FollowUnfollowResponse>(environment.resourceUrl + '/user-connections/unfollow', {targetUserId: targetUserId})
  }

}
