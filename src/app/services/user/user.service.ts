import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {User} from "../../models/user";
import {map} from "rxjs/operators";


@Injectable({providedIn: "root"})
export class UserService {

  private userDetailsSubject = new ReplaySubject<User>();

  constructor(private httpClient: HttpClient) {
  }

  public getUserDetailsSubject(): Observable<User> {
    return this.userDetailsSubject.asObservable();
  }

  getUserDetails() {
    this.httpClient
      .get<User>(environment.resourceUrl + '/users/me')
      .subscribe(response => {
        this.userDetailsSubject.next(response);
      });
  }

  updateUserDetails(user: User) {
    return this.httpClient
      .post<User>(environment.resourceUrl + '/users/me', user);
  }

  updateUserProfilePicture(file: File) {
    const imageUploadPath = environment.resourceUrl + '/users/upload-profile-image';
    const formData = new FormData();
    formData.append("file", file, file.name);
    return this.httpClient.post<{ message: string, upload: string }>(imageUploadPath, formData)
      .pipe(map(fileUploadResponse => {
        console.log('file upload response');
        console.log(fileUploadResponse);
        return fileUploadResponse.upload;
      }));
  }

  getProfileImage(imageId: String) {
    if (imageId === undefined) {
      return 'https://www.kindpng.com/picc/m/78-786207_user-avatar-png-user-avatar-icon-png-transparent.png';
    }
    return environment.resourceUrl + "/users/profile-images/" + imageId;
  }

}
