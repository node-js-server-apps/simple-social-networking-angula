import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {UserList} from "../../models/user.list";


@Injectable({providedIn: "root"})
export class UserSuggestionsService {

  private usersSubject = new Subject<UserList>();

  constructor(private httpClient: HttpClient) {
  }

  public getSuggestionsSubject(): Observable<UserList> {
    return this.usersSubject.asObservable();
  }

  getSuggestions() {
    this.httpClient
      .get<UserList>(environment.resourceUrl + '/user-connections/me/suggestions')
      .subscribe(response => {
        this.usersSubject.next(response);
      });
  }

}
