import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HomeTweets} from "../../models/home.tweets";
import {Tweet} from "../../models/tweet";


@Injectable({providedIn: "root"})
export class TweetSearchService {

  private tweetSearchSubject = new Subject<Tweet[]>();

  constructor(private httpClient: HttpClient) {
  }

  public getTweetSearchUpdateListener(): Observable<Tweet[]> {
    return this.tweetSearchSubject.asObservable();
  }

  searchTweets(tags: string) {
    if (tags) {
      this.httpClient
        .get<HomeTweets>(environment.resourceUrl + '/search/tags?value=' + tags)
        .subscribe(response => {
          this.tweetSearchSubject.next(response.tweets);
        });
    }
  }

  searchTweetsByText(text: String) {
    this.httpClient
      .post<HomeTweets>(environment.resourceUrl + '/search/text', {query: text})
      .subscribe(response => {
        this.tweetSearchSubject.next(response.tweets);
      });
  }

  searchDefault() {
    this.httpClient
      .get<HomeTweets>(environment.resourceUrl + '/tweets')
      .subscribe(response => {
        this.tweetSearchSubject.next(response.tweets);
      });
  }

}
