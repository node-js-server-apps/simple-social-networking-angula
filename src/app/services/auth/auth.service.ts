import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {StorageService} from "../storage/storage.service";
import {TokenResponse} from "../../models/token.response";
import {RegisterUser} from "../../models/register-user";


@Injectable()
export class AuthService {

  constructor(private httpClient: HttpClient,
              private storageService: StorageService) {
  }

  login(username: string, password: string): Observable<boolean> {
    let httpHeader: HttpHeaders = new HttpHeaders();
    httpHeader = httpHeader.append('Content-Type', 'application/json');
    const loginRequest = {"username": username, "password": password};
    return this.httpClient
      .post<TokenResponse>(environment.resourceUrl + '/auth/login', loginRequest, {headers: httpHeader})
      .pipe(map(resp => {
        this.storageService.storeToken(JSON.stringify(resp));
        return true;
      }));
  }


  isLoggedIn() {
    return this.storageService.isLoggedIn();
  }

  logout() {
    this.storageService.clear();
  }

  register(user: RegisterUser): Observable<boolean> {
    return this.httpClient
      .post<boolean>(environment.resourceUrl + '/auth/register', user)
      .pipe(map(resp => {
        this.storageService.storeToken(JSON.stringify(resp));
        return true;
      }));
  }

  getBearerAccessToken(): string {
    return this.storageService.getToken();
  }
}
