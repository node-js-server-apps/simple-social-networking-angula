import {HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {AuthService} from './auth.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {AuthGuardService} from './auth.guard.service';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService,
              private authGuard: AuthGuardService,
              private router: Router) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // console.log('Can activate: ' + this.authGuard.canActivate());

    if (this.authGuard.canActivate()) {
      if (this.authService.isLoggedIn()) {
        let authReq = req;
        const token = this.authService.getBearerAccessToken();
        authReq = authReq.clone({headers: req.headers.set('Authorization', 'Bearer ' + token)});
        return next.handle(authReq);
      } else {
        return next.handle(req);
      }
    } else {
      return next.handle(req);
    }
  }
}

export const AppAuthInterceptorProviders = [
  {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
];
