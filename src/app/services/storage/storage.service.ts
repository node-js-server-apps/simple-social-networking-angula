import {Injectable} from '@angular/core';
import {TokenResponse} from "../../models/token.response";
import jwtDecode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  AUTH_TOKEN = 'AUTH_TOKEN';

  constructor() {
  }


  public storeToken(token: string) {
    localStorage.setItem(this.AUTH_TOKEN, token);
  }

  private getTokenObject() {
    return localStorage.getItem(this.AUTH_TOKEN);
  }

  public isLoggedIn(): boolean {
    const token = this.getToken();
    if (token !== null && token !== "") {
      const decodedData: any = jwtDecode(token);
      if (Date.now() >= decodedData.exp * 1000) {
        this.clear();
        return false;
      } else {
        return true;
      }
    }
    return false;
  }

  public getToken() {
    const tokenString = this.getTokenObject();
    if (tokenString != null) {
      const token: TokenResponse = JSON.parse(tokenString);
      return token.token;
    } else {
      return "";
    }
  }


  clear() {
    localStorage.removeItem('AUTH_TOKEN');
  }
}
