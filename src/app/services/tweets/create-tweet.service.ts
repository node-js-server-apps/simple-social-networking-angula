import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {CreateTweetRequest} from "../../models/create-tweet-request";
import {CreateTweetResponse} from "../../models/create-tweet-response";
import {concatMap, map} from "rxjs/operators";


@Injectable({providedIn: "root"})
export class CreateTweetService {


  constructor(private httpClient: HttpClient) {
  }

  createTweet(newTweet: CreateTweetRequest) {
    return this.httpClient
      .post<CreateTweetResponse>(environment.resourceUrl + '/tweets', newTweet);
  }

  createMultiMediaTweet(newTweet: CreateTweetRequest, file: File) {
    const api1 = this.createTweet(newTweet);
    const api2Url = environment.resourceUrl + '/tweets/upload-media/';
    return api1.pipe(concatMap(api1Res => {
      console.log('api 1 resposne');
      console.log(api1Res);
      const formData = new FormData();
      formData.append("file", file, file.name);
      return this.httpClient.post<{ message: string, upload: any }>(api2Url + api1Res.tweet.id, formData)
        .pipe(map(fileUploadResponse => {
          console.log('file upload response');
          console.log(fileUploadResponse);
          api1Res.tweet.media = [fileUploadResponse.upload.id]
          return api1Res;
        }))
    }))
      .pipe(map(api2Resp => {
        console.log('api 2 resposne');
        console.log(api2Resp);
        return api2Resp;
      }));
  }
}
