import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HomeTweets} from "../../models/home.tweets";


@Injectable({providedIn: "root"})
export class ReplyDeleteService {


  constructor(private httpClient: HttpClient) {
  }

  deleteTweetReply(tweetId: string, replyId: string) {
    return this.httpClient.delete<HomeTweets>(environment.resourceUrl + '/tweets/' + tweetId + '/reply/' + replyId);
  }

}
