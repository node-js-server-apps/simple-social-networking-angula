import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {StorageService} from "../storage/storage.service";
import {TweetDeleteResponse} from "../../models/tweet.delete.response";
import {Observable} from "rxjs";


@Injectable({providedIn: "root"})
export class TweetDeleteService {


  constructor(private httpClient: HttpClient,
              private storageService: StorageService) {
  }

  deleteTweet(id: string): Observable<TweetDeleteResponse> {
    return this.httpClient.delete<TweetDeleteResponse>(environment.resourceUrl + '/tweets/' + id);
  }

}
