import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HomeTweets} from "../../models/home.tweets";
import {Tweet} from "../../models/tweet";


@Injectable({providedIn: "root"})
export class MyTweetService {

  private myTweetSubject = new Subject<Tweet[]>();

  constructor(private httpClient: HttpClient) {
  }

  public getMyTweetsUpdateListener(): Observable<Tweet[]> {
    return this.myTweetSubject.asObservable();
  }

  fetchTweets() {
    this.httpClient
      .get<HomeTweets>(environment.resourceUrl + '/tweets/me')
      .subscribe(response => {
        this.myTweetSubject.next(response.tweets);
      });
  }

}
