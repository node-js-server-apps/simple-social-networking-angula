import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {StorageService} from "../storage/storage.service";
import {HomeTweets} from "../../models/home.tweets";
import {Tweet} from "../../models/tweet";


@Injectable({providedIn: "root"})
export class TweetService {

  private tweetSubject = new Subject<Tweet[]>();

  constructor(private httpClient: HttpClient,
              private storageService: StorageService) {
  }

  public getTweetsUpdateListener(): Observable<Tweet[]> {
    return this.tweetSubject.asObservable();
  }

  fetchTweets() {
    this.httpClient
      .get<HomeTweets>(environment.resourceUrl + '/tweets/timeline')
      .subscribe(response => {
        this.tweetSubject.next(response.tweets);
      });
  }

}
