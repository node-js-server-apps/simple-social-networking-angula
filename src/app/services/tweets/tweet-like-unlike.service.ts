import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from "rxjs";
import {ApiResponse} from "../../models/api.response";


@Injectable({providedIn: "root"})
export class TweetLikeUnlikeService {


  constructor(private httpClient: HttpClient) {
  }

  likeTweet(tweetId: string): Observable<ApiResponse> {
    return this.httpClient.patch<ApiResponse>(environment.resourceUrl + '/tweets/' + tweetId + '/like', null);
  }


  unLikeTweet(tweetId: string): Observable<ApiResponse> {
    return this.httpClient.patch<ApiResponse>(environment.resourceUrl + '/tweets/' + tweetId + '/unlike', null);
  }

}
