import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {StorageService} from "../storage/storage.service";


@Injectable({providedIn: "root"})
export class TweetCountService {

  private tweetCountSubject = new Subject<{ count: number }>();

  constructor(private httpClient: HttpClient,
              private storageService: StorageService) {
  }

  public getTweetCountSubject(): Observable<{ count: number }> {
    return this.tweetCountSubject.asObservable();
  }

  getMyTweetCount() {
    this.httpClient
      .get<{ count: number }>(environment.resourceUrl + '/tweets/count')
      .subscribe(response => {
        this.tweetCountSubject.next(response);
      });
  }

}
