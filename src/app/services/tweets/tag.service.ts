import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Tag} from "../../models/tag";
import {HomeTags} from "../../models/home.tags";


@Injectable({providedIn: "root"})
export class TagService {

  private tagsSubject = new Subject<Tag[]>();

  constructor(private httpClient: HttpClient) {
  }

  public getTagsUpdateListener(): Observable<Tag[]> {
    return this.tagsSubject.asObservable();
  }

  fetchTags() {
    this.httpClient
      .get<HomeTags>(environment.resourceUrl + '/tags/recent')
      .subscribe(response => {
        this.tagsSubject.next(response.tags);
      });
  }

}
