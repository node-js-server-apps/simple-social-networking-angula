import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Reply} from "../../models/reply";
import {CreateReplyResponse} from "../../models/create-reply-response";


@Injectable({providedIn: "root"})
export class ReplyTweetService {


  constructor(private httpClient: HttpClient) {
  }

  replyToTweet(id: string, reply: Reply) {
    return this.httpClient
      .post<CreateReplyResponse>(environment.resourceUrl + '/tweets/' + id + '/reply', reply);

  }

}
