import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutModule} from '@angular/cdk/layout';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {LoginComponent} from './features/login/login.component';
import {RegisterComponent} from './features/register/register.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatCardModule} from "@angular/material/card";
import {MatInputModule} from "@angular/material/input";
import {AuthService} from "./services/auth/auth.service";
import {HttpClientModule} from "@angular/common/http";
import {HeaderComponent} from './features/header/header.component';
import {FooterComponent} from './features/footer/footer.component';
import {HomeComponent} from './features/home/home.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {AllTagsComponent} from './features/all-tags/all-tags.component';
import {AppAuthInterceptorProviders} from "./services/auth/auth.interceptor";
import {MyTweetsComponent} from './features/my-tweets/my-tweets.component';
import {TimelineComponent} from './features/timeline/timeline.component';
import {ProfileComponent} from './features/profile/profile.component';
import {TweetComponent} from './components/tweet/tweet.component';
import {SidebarRightComponent} from './features/sidebar-right/sidebar-right.component';
import {SidebarLeftComponent} from './features/sidebar-left/sidebar-left.component';
import {TagComponent} from './components/tag/tag.component';
import {TagSearchComponent} from './features/tag-search/tag-search.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DateAgoPipe} from "./utils/date-time-ago";
import {NgPipesModule} from "ngx-pipes";
import {CreateTweetComponent} from './components/create-tweet/create-tweet.component';
import {AppInfoHeaderComponent} from './features/aoo-info-header/app-info-header.component';
import {NgBootstrapFormValidationModule} from "ng-bootstrap-form-validation";
import {SettingsComponent} from './features/settings/settings.component';
import {MyImagesComponent} from './features/my-images/my-images.component';
import {TweetReplyFormComponent} from './components/tweet-reply-form/tweet-reply-form.component';
import {TweetRepliesComponent} from './components/tweet-replies/tweet-replies.component';
import {NavigationHeaderComponent} from './features/navigation-header/navigation-header.component';
import {GlobalSearchComponent} from './components/global-search/global-search.component';
import {TweetDetailsComponent} from './components/tweet-details/tweet-details.component';
import {LandingComponent} from './features/landing/landing.component';
import {FollowersComponent} from './features/followers/followers.component';
import {UserDetailSmallComponent} from './components/user-detail-small/user-detail-small.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {NotFoundLandingComponent} from './features/not-found-landing/not-found-landing.component';
import {FollowingsComponent} from './features/followings/followings.component';
import {EmptyComponentComponent} from './components/empty-component/empty-component.component';
import {WhoToFollowSuggestionsComponent} from './features/who-to-follow-suggestions/who-to-follow-suggestions.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AllTagsComponent,
    MyTweetsComponent,
    TimelineComponent,
    ProfileComponent,
    TweetComponent,
    SidebarRightComponent,
    SidebarLeftComponent,
    TagComponent,
    TagSearchComponent,
    DateAgoPipe,
    CreateTweetComponent,
    AppInfoHeaderComponent,
    SettingsComponent,
    MyImagesComponent,
    TweetReplyFormComponent,
    TweetRepliesComponent,
    NavigationHeaderComponent,
    GlobalSearchComponent,
    TweetDetailsComponent,
    LandingComponent,
    FollowersComponent,
    UserDetailSmallComponent,
    NotFoundComponent,
    NotFoundLandingComponent,
    FollowingsComponent,
    EmptyComponentComponent,
    WhoToFollowSuggestionsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    ReactiveFormsModule,
    MatInputModule,
    FlexLayoutModule,
    HttpClientModule,
    NgbModule,
    NgPipesModule,
    NgBootstrapFormValidationModule.forRoot(),
    NgBootstrapFormValidationModule,
  ],
  providers: [
    AuthService,
    AppAuthInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
