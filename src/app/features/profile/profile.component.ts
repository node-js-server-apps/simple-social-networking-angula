import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from "../../models/user";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user/user.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  user: User;
  form: FormGroup;
  isLoading = false;
  selectedFile: File | null = null
  imageId: String;

  constructor(private fb: FormBuilder,
              private userService: UserService) {
    this.form = this.fb.group({
      fullname: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(12)
      ])
    });
  }

  ngOnInit(): void {
    this.fetchUserDetails();
  }

  private fetchUserDetails() {
    this.userService.getUserDetailsSubject()
      .subscribe(user => {
        this.form.setValue({'fullname': user.name});
        this.user = user;
        this.imageId = user?.mediaId;
      }, error => {
        console.error(error);
      });
    this.userService.getUserDetails();
  }

  onSubmit() {
    this.isLoading = !this.isLoading;
    const fullname = this.form.get('fullname')?.value;
    const newUser = new User();
    newUser.name = fullname;
    this.userService.updateUserDetails(newUser)
      .subscribe(updatedUser => {
        this.isLoading = !this.isLoading;
        this.user = updatedUser;
      }, error => {
        console.error(error);
        this.isLoading = !this.isLoading;
        console.error('cannot update user');
      })
  }

  onFileSelected(event: any) {
    const reader = new FileReader();
    const file: File = event.target.files[0];

    if (file) {
      this.selectedFile = file;
      this.userService.updateUserProfilePicture(this.selectedFile)
        .subscribe(mediaId => {
          console.log('profile picture success');
          this.imageId = mediaId;
        }, error => {
          console.log('error uploading image for user profile picture');
        })
    } else {
      console.log('selected file no defined');
    }
  }

  getMyProfilePicture() {
    return this.userService.getProfileImage(this.imageId);
  }

  ngOnDestroy(): void {
  }

}
