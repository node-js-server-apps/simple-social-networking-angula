import {Component, OnInit} from '@angular/core';
import {UserService} from "../../services/user/user.service";
import {UserConnectionService} from "../../services/user-connection/user-connection.service";
import {User} from "../../models/user";

@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.scss']
})
export class FollowersComponent implements OnInit {
  page = 0;
  pageSize = 10
  followers: User[] = []
  loggedInUser: User;

  constructor(private userService: UserService,
              private userConnectionService: UserConnectionService) {
  }

  ngOnInit(): void {
    this.subscribeForUserDetails();
  }

  private subscribeForUserDetails() {
    this.userService.getUserDetailsSubject()
      .subscribe(resp => {
        this.loggedInUser = resp;
        this.subscribeForUserFollowersFollowings();
        this.userConnectionService.getMyConnection();
      }, error => {
        console.log('error getting user details in UserDetailsSmallComponent')
      });
    this.userService.getUserDetails();
    console.log('getting user details');
  }

  private subscribeForUserFollowersFollowings() {
    this.userConnectionService.getUserConnectionSubject()
      .subscribe(resp => {
        this.followers = resp.followers;
        console.log('received data yay');
      }, error => {
        console.log('cannot get followers/followings');
      });
  }

}
