import {Component, OnDestroy, OnInit} from '@angular/core';
import {Tag} from "../../models/tag";
import {TagService} from "../../services/tweets/tag.service";
import {Subscription} from "rxjs";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-all-tags',
  templateUrl: './all-tags.component.html',
  styleUrls: ['./all-tags.component.scss']
})
export class AllTagsComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  tags: Tag[] = [];

  constructor(private tagService: TagService) {
  }

  ngOnInit(): void {
    this.tagService.fetchTags();
    this.subscription = this.tagService.getTagsUpdateListener()
      .subscribe(resp => {
        this.tags = resp;
      }, error => {
        // console.log(error);
      })

  }

  getLink(name: String) {
    const tagName = name.replace('#', '');
    return environment.selfHost + '/search/tags?tags=' + tagName;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

}
