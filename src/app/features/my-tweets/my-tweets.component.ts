import {Component, OnDestroy, OnInit} from '@angular/core';
import {Tweet} from "../../models/tweet";
import {Subscription} from "rxjs";
import {MyTweetService} from "../../services/tweets/my-tweet.service";

@Component({
  selector: 'app-my-tweets',
  templateUrl: './my-tweets.component.html',
  styleUrls: ['./my-tweets.component.scss']
})
export class MyTweetsComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  tweets: Tweet[] = [];

  constructor(private myTweetService: MyTweetService) {
  }

  ngOnInit(): void {
    this.subscription = this.myTweetService.getMyTweetsUpdateListener()
      .subscribe(resp => {
        this.tweets = resp;
      }, error => {
        // console.log(error);
      })
    this.myTweetService.fetchTweets();
  }

  onTweetCreated(tweet: Tweet) {
    this.tweets.unshift(tweet);
  }


  removeTweetFromList(tweet: Tweet) {
    console.log('remove tweet from list: ' + tweet.content);
    this.tweets = this.tweets.filter(x => x.id !== tweet.id);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

}
