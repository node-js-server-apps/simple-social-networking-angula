import {Component, OnInit} from '@angular/core';
import {environment} from "../../../environments/environment";
import {AuthService} from "../../services/auth/auth.service";
import {User} from "../../models/user";
import {UserService} from "../../services/user/user.service";
import {TweetService} from "../../services/tweets/tweet.service";
import {TweetCountService} from "../../services/tweets/tweet-count.service";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  user: User;
  myTweetCount = 0

  constructor(private userService: UserService,
              private authService: AuthService,
              private tweetService: TweetService,
              private tweetCountService: TweetCountService) {
  }

  ngOnInit(): void {
    this.fetchUserDetails();
    this.fetchTweetsCount();
  }

  private fetchUserDetails() {
    this.userService.getUserDetailsSubject()
      .subscribe(user => {
        this.user = user;
      }, error => {
        console.error(error);
      });
    this.userService.getUserDetails();
  }

  private fetchTweetsCount() {
    this.tweetCountService.getTweetCountSubject()
      .subscribe(response => {
        this.myTweetCount = response.count;
      }, error => {
        console.error(error);
      });
    this.tweetCountService.getMyTweetCount();
  }

  logout() {
    this.authService.logout();
    window.location.href = environment.selfHost + '/login';
  }

  ngOnDestroy(): void {
  }


}
