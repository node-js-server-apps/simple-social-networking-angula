import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AppInfoHeaderComponent} from './app-info-header.component';

describe('AooInfoHeaderComponent', () => {
  let component: AppInfoHeaderComponent;
  let fixture: ComponentFixture<AppInfoHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppInfoHeaderComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppInfoHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
