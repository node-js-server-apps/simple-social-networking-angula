import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth/auth.service";
import {environment} from "../../../environments/environment";
import {RegisterUser} from "../../models/register-user";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;
  loginInvalid: boolean = false;

  constructor(private fb: FormBuilder, private authService: AuthService) {
    this.form = this.fb.group({
      fullname: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(12)
      ]),
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(12)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(20)
      ])
    });
  }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      window.location.href = environment.selfHost + '/';
    }
  }


  onSubmit() {
    const fullname = this.form.get('fullname')?.value;
    const username = this.form.get('username')?.value;
    const password = this.form.get('password')?.value;
    const newUser = new RegisterUser(fullname, username, password)
    this.authService.register(newUser)
      .subscribe(registered => {
        window.location.href = environment.selfHost + '/home';
      }, error => {
        // console.error(error);
      })
  }
}
