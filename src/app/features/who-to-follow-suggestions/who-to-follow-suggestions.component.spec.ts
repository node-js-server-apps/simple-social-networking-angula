import {ComponentFixture, TestBed} from '@angular/core/testing';

import {WhoToFollowSuggestionsComponent} from './who-to-follow-suggestions.component';

describe('WhoToFollowSuggestionsComponent', () => {
  let component: WhoToFollowSuggestionsComponent;
  let fixture: ComponentFixture<WhoToFollowSuggestionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WhoToFollowSuggestionsComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WhoToFollowSuggestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
