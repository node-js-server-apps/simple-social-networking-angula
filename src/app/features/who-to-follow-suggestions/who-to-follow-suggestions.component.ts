import {Component, OnInit} from '@angular/core';
import {User} from "../../models/user";
import {UserSuggestionsService} from "../../services/whotofollowsuggestions/UserSuggestionsService";

@Component({
  selector: 'app-who-to-follow-suggestions',
  templateUrl: './who-to-follow-suggestions.component.html',
  styleUrls: ['./who-to-follow-suggestions.component.scss']
})
export class WhoToFollowSuggestionsComponent implements OnInit {
  users: User[] = [];

  constructor(private userSuggestionsService: UserSuggestionsService) {
  }

  ngOnInit(): void {
    this.subscribeForData();
    this.getUserSuggestions();
  }

  private subscribeForData() {
    this.userSuggestionsService.getSuggestionsSubject()
      .subscribe(data => {
        this.users = data.users;
      }, error => {
        console.error('Error getting user suggestions');
      })
  }

  private getUserSuggestions() {
    this.userSuggestionsService.getSuggestions();
  }

  removeUserFromSuggestions(user: User) {
    this.users = this.users.filter(x => x.id !== user.id);
  }

}
