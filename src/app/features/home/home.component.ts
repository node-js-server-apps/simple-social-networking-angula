import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {TweetService} from "../../services/tweets/tweet.service";
import {Subscription} from "rxjs";
import {Tweet} from "../../models/tweet";
import {UserService} from "../../services/user/user.service";
import {User} from "../../models/user";
import {TweetCountService} from "../../services/tweets/tweet-count.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  @Input()
  tweetCreatedEvent: Tweet;
  private subscription: Subscription;
  tweets: Tweet[] = [];
  user: User;
  myTweetCount = 0

  constructor(private router: Router,
              private userService: UserService,
              private tweetService: TweetService,
              private tweetCountService: TweetCountService) {
  }

  ngOnInit(): void {
    this.tweetService.fetchTweets();
    this.subscription = this.tweetService.getTweetsUpdateListener()
      .subscribe(resp => {
        this.tweets = resp;
      }, error => {
        // console.log(error);
      })
    this.fetchUserDetails();
    this.fetchTweetsCount();
  }

  onTweetCreated(tweet: Tweet) {
    this.tweets.unshift(tweet);
  }

  removeTweetFromHomeList(tweet: Tweet) {
    console.log('remove tweet from list: ' + tweet.content);
    this.tweets = this.tweets.filter(x => x.id !== tweet.id);
  }


  private fetchUserDetails() {
    this.userService.getUserDetailsSubject()
      .subscribe(user => {
        this.user = user;
      }, error => {
        console.error(error);
      });
    this.userService.getUserDetails();
  }

  private fetchTweetsCount() {
    this.tweetCountService.getTweetCountSubject()
      .subscribe(response => {
        this.myTweetCount = response.count;
      }, error => {
        console.error(error);
      });
    this.tweetCountService.getMyTweetCount();
  }

  removeTweetFromList(tweet: Tweet) {
    console.log('remove tweet from list: ' + tweet.content);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }


  showDetailView(tweet: Tweet) {
    console.log('item home clicked');
    this.router.navigate(['/tweet-details'],
      {queryParams: {tweet: JSON.stringify(tweet)}});

  }
}
