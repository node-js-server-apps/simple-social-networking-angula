import {Component, OnInit} from '@angular/core';
import {User} from "../../models/user";
import {UserService} from "../../services/user/user.service";
import {UserConnectionService} from "../../services/user-connection/user-connection.service";

@Component({
  selector: 'app-followings',
  templateUrl: './followings.component.html',
  styleUrls: ['./followings.component.scss']
})
export class FollowingsComponent implements OnInit {
  page = 0;
  pageSize = 10
  numbers: number[] = [];
  followings: User[] = []
  loggedInUser: User;

  constructor(private userService: UserService,
              private userConnectionService: UserConnectionService) {
    for (let i = 0; i < 100; i++) {
      this.numbers.push(i);
    }
  }

  ngOnInit(): void {
    this.subscribeForUserDetails();
    this.userService.getUserDetails();
  }

  private subscribeForUserDetails() {
    this.userService.getUserDetailsSubject()
      .subscribe(resp => {
        this.loggedInUser = resp;
        this.subscribeForUserFollowersFollowings();
        this.userConnectionService.getMyConnection();
      }, error => {
        console.error('error getting user details in UserDetailsSmallComponent')
      });

  }

  private subscribeForUserFollowersFollowings() {
    this.userConnectionService.getUserConnectionSubject()
      .subscribe(resp => {
        this.followings = resp.followings;
      }, error => {
        console.error('cannot get followers/followings');
      });
  }

  removeUserFromFollowings(user: User) {
    this.followings = this.followings.filter(x => x.id !== user.id);
  }

}
