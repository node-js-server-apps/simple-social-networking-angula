import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth/auth.service";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  loginInvalid: boolean = false;

  constructor(private fb: FormBuilder, private authService: AuthService) {
    this.form = this.fb.group({
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(12)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(20)
      ])
    });
  }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      window.location.href = environment.selfHost + '/';
    }
  }


  onSubmit() {
    const username = this.form.get('username')?.value;
    const password = this.form.get('password')?.value;
    this.authService.login(username, password)
      .subscribe(login => {
        window.location.href = environment.selfHost + '/home';
      }, error => {
        // console.error(error);
      })
  }

}
