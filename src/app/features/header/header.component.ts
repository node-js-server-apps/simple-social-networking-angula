import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {Tweet} from "../../models/tweet";
import {User} from "../../models/user";
import {UserService} from "../../services/user/user.service";
import {TweetCountService} from "../../services/tweets/tweet-count.service";
import {UserConnectionService} from "../../services/user-connection/user-connection.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  tweets: Tweet[] = [];
  user: User;
  myTweetCount = 0;
  followers = 0;
  followings = 0;

  constructor(private activatedRoute: ActivatedRoute,
              private userService: UserService,
              private userConnectionService: UserConnectionService,
              private tweetCountService: TweetCountService) {
  }

  ngOnInit(): void {
    this.fetchUserDetails();
    this.fetchTweetsCount();
    this.fetchUserConnections();
  }

  getCurrentActiveRoute() {
    return this.activatedRoute.snapshot.url[0].path
  }

  private fetchUserDetails() {
    this.subscriptions.push(
      this.userService.getUserDetailsSubject()
        .subscribe(user => {
          this.user = user;
        }, error => {
          console.error(error);
        }));
    this.userService.getUserDetails();
  }

  private fetchTweetsCount() {
    this.subscriptions.push(
      this.tweetCountService.getTweetCountSubject()
        .subscribe(response => {
          this.myTweetCount = response.count;
        }, error => {
          console.error(error);
        }));
    this.tweetCountService.getMyTweetCount();
  }

  private fetchUserConnections() {
    this.subscriptions.push(
      this.userConnectionService.getUserConnectionSubject()
        .subscribe(response => {
          this.followers = response.followers.length;
          this.followings = response.followings.length;
        }, error => {
          // console.error(error);
        }));
    this.userConnectionService.getMyConnection();
  }


  ngOnDestroy(): void {
    this.subscriptions.map(subscription => {
      subscription.unsubscribe();
    });
  }
}
