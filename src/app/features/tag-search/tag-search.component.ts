import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {Tweet} from "../../models/tweet";
import {ActivatedRoute} from "@angular/router";
import {TweetSearchService} from "../../services/search/tweet-search.service";

@Component({
  selector: 'app-search',
  templateUrl: './tag-search.component.html',
  styleUrls: ['./tag-search.component.scss']
})
export class TagSearchComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  tweets: Tweet[] = [];
  searchTags = '';
  searchQuery = ''

  constructor(private searchService: TweetSearchService,
              private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      this.searchTags = params['tags'];
      this.searchQuery = params['query'];
    });
  }

  ngOnInit(): void {
    this.subscription = this.searchService.getTweetSearchUpdateListener()
      .subscribe(resp => {
        this.tweets = resp;
      }, error => {
        // console.log(error);
      });
    if (this.searchTags) {
      this.searchService.searchTweets(this.searchTags);
      return;
    }
    if (this.searchQuery) {
      this.searchService.searchTweetsByText(this.searchQuery);
      console.log('this is search query: ' + this.searchQuery);
      return;
    }

    this.searchService.searchDefault();

  }


  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }
}
